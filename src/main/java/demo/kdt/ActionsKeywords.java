package demo.kdt;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ActionsKeywords {
    private static final Object True = null;
	WebDriver driver;
    WebElement element;

    public void TextBoxType(String parameter, String value,String data){
        if (parameter.equals("xpath")){
           element = By.xpath(value).findElement(driver);
        }
        if (parameter.equals("id")){
            element = By.id(value).findElement(driver);
        }
        element.sendKeys(data);
        element.sendKeys(Keys.RETURN);
    }
    public void LinkClick(String parameter, String value){
        if (parameter.equals("xpath")){
            element = By.xpath(value).findElement(driver);
        }
        if (parameter.equals("id")){
            element = By.id(value).findElement(driver);
        }
        element.click();
    }
    public void ButtonClick(String parameter, String value){
        if (parameter.equals("xpath")){
            element = By.xpath(value).findElement(driver);
        }
        if (parameter.equals("id")){
            element = By.id(value).findElement(driver);
        }
        element.click();
    }
    public void OpenBrowser(String navegador,String parameter) {
        if (navegador.equals("Firefox")) {
            driver = new FirefoxDriver();
        }else{
        	driver = new ChromeDriver();
        }
        driver.get(parameter);
        driver.manage().window().maximize();
    }
    public void CloseBrowser(){
        driver.close();
    }
}
