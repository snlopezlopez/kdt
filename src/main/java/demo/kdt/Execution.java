package demo.kdt;

import org.apache.poi.ss.usermodel.Row;
import demo.kdt.ActionsKeywords;

import java.io.IOException;

public class Execution {

    //Main function is calling readExcel function to read data from excel file

    public static void main(String... strings) throws IOException {

        //Create an object of ReadGuru99ExcelFile class

        ExcelUtility objExcelFile = new ExcelUtility();
        ActionsKeywords actionsKeyword = new ActionsKeywords();

        //Prepare the path of excel file


        String filePath = "src\\main\\resources\\";

        //Call read file method of the class to read data

        //objExcelFile.readExcel(filePath, "kdt.xlsx", "Plan");

        int rows = objExcelFile.numberOfRows(filePath, "kdt.xlsx", "Plan");
        for (int i = 0; i < rows+1; i++) {

            Row row = objExcelFile.readRow(filePath, "kdt.xlsx", "Plan",i);

            //Create a loop to print cell values in a row

            for (int j = 0; j < row.getLastCellNum(); j++) {

                //Print Excel data in console
                if (row.getCell(j).getStringCellValue().equals("OpenBrowser")){
                    actionsKeyword.OpenBrowser(row.getCell(j+3).getStringCellValue(), row.getCell(j+2).getStringCellValue());
                }
                if (row.getCell(j).getStringCellValue().equals("TextBoxType")){
                    actionsKeyword.TextBoxType(row.getCell(j+1).getStringCellValue(), row.getCell(j+2).getStringCellValue(),row.getCell(j+3).getStringCellValue());
                }
                if (row.getCell(j).getStringCellValue().equals("LinkClick")){
                    actionsKeyword.LinkClick(row.getCell(j+1).getStringCellValue(), row.getCell(j+2).getStringCellValue());
                }
                if (row.getCell(j).getStringCellValue().equals("CloseBrowser")){
                    actionsKeyword.CloseBrowser();
                }

                System.out.print(row.getCell(j).getStringCellValue()+"|| ");
            }

            System.out.println();
        }
    }

}